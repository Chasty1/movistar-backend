# POC - CRM - LITE - API


- Swagger available on http://localhost:3000/api-docs
- Routes can be protected with **JWT tokens**.
- Authentification with Passport. 

# How it works

- The API dispatches requests with well structured **routes**.
- Routes are using **controllers** for API implementations.
- Controllers are using **models** for Mongo persistence.
- Routes can be protected with **JWT authentification middelwares** :
```typescript
    public router: Router;
    public authController: AuthController = new AuthController();
    public representantesController: RepresentantesController = new RepresentantesController();
    public getComprasController: GetComprasController = new GetComprasController();
    public getCuentaCorrienteController: GetCuentaCorrienteController = new GetCuentaCorrienteController();
    public getCustomersController: GetCustomersController = new GetCustomersController();
    public getSubscribersController: GetSubscribersController = new GetSubscribersController();

    constructor() {
        this.router = Router();
        this.routes();
    }
    routes() {
        /*        */
        this.router.get("/api.rodar/reps/:id", this.authController.authenticateJWT, this.representantesController.getRepresentantes);
        /*        */
        this.router.get("/api.rodar/replace_offer", this.authController.authenticateJWT, this.getComprasController.getCompras);
        /*        */
        this.router.get("/api.rodar/finantial_accounts/:customer_id", this.authController.authenticateJWT, this.getCuentaCorrienteController.getCuentaCorriente);
        /*        */
        this.router.get("/api.rodar/customers/:id", this.authController.authenticateJWT, this.getCustomersController.getCustomers);
        /*        */
        this.router.get("/api.rodar/subscribers/:customer_id", this.authController.authenticateJWT, this.getSubscribersController.getSubscribers);
        /*        */
        this.router.post("/api.rodar/replace_offer", this.authController.authenticateJWT, this.getComprasController.postCompras);
    }
```

# Installation
- Clone the repository
```
git clone https://gitlab.com/poc-crm-lite/poc-crm-lite.git
```
- Install dependencies
```
cd poc-crm-lite
npm install
npm run build
```
- Launch demo Node and Mongo server in docker containers
```
docker-compose build
docker-compose up
```
( *Alternatively, you can run and configure your local or cloud Mongo server and start Node server with
`npm run build && npm start`*)

Please check package.json for other useful npm scripts  (for example typescript and nodemon watchers in development)


# Getting started


## Step1 : Register a user
Send a POST request to `http://localhost:3000/api/user/register` 
with the following payload ** :
```json
{
    "username": "user1",
    "password": "pass1",
    "rol": "Retencion"
}
```

or

```json
{
    "username": "user2",
    "password": "pass2",
    "rol": "Callcenter"
}
```


You should get a JWT token in the response :
```json
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMiIsImlhdCI6MTU1MDU4MTA4NH0.WN5D-BFLypnuklvO3VFQ5ucDjBT68R2Yc-gj8AlkRAs"
}
```

## Step2 : Login a user
Send a POST request to `http://localhost:3000/api/user/login` 
with the following payload ** :
```json
{
    "username": "user1",
    "password": "pass1"
}
```
or

```json
{
    "username": "user2",
    "password": "pass2"
}
```
You should get a JWT token in the response :
```json
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMiIsImlhdCI6MTU1MDU4MTA4NH0.WN5D-BFLypnuklvO3VFQ5ucDjBT68R2Yc-gj8AlkRAs"
}
```





