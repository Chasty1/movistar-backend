import { logger } from "../../configuration/log4js";
import { Request } from "express";
import compraMock from "../../mocks/compraMock";

const postSOSUrl = "URLMOCKPOST";

function mapCompras(id: number) {
    return compraMock.items.filter(singleCompra => singleCompra.id === id);
}

const postCompra = async (req: Request) => {
    try {

        const data = mapCompras(req.body.item);
        if (data.length) {
            logger.info(`\nURL: ${postSOSUrl}\nMETHOD: POST\nDATA: ${JSON.stringify(data)}`);
            return data;
        } else {
            logger.info(`\nURL: ${postSOSUrl}\nMETHOD: POST\nDATA: ${JSON.stringify(data)}`);
            return false;
        }


    } catch (e) {
        logger.error(`\nURL: ${postSOSUrl}\nMETHOD: POST\nERROR: ${e.message} - ${e.response.statusText}`);
        return {
            data: {
                URL: postSOSUrl,
                METHOD: "POST",
                ERROR: (e.response.data && e.response.data.code) ? `${e.message} - ${e.response.statusText} - ${e.response.data.code}` : `${e.message} - ${e.response.statusText}`,
                MESSAGE: (e.response.data && e.response.data.message) ? `${e.response.data.message}` : ""
            },
            status: e.response.status
        };
    }
};

export default { postCompra };
