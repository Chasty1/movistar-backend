import bcrypt from "bcrypt-nodejs";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import passport from "passport";
import "../auth/passportHandler";
import { User } from "../models/user";
import { JWT_SECRET } from "../util/secrets";

export class UserController {


    public async registerUser(req: Request, res: Response): Promise<void> {
          // const hashedPassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8));

        await User.create({
            username: req.body.username,
            password: req.body.password,
            rol: req.body.rol
        });

        const token = jwt.sign({ username: req.body.username,  scope: req.body.scope }, JWT_SECRET);
        res.status(200).send({ token: token });
    }

    public authenticateUser(req: Request, res: Response, next: NextFunction) {
        console.log("authenticate");
        User.findOne({
            username: req.body.username
        })
            .exec((err, user) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }
                if (!user) {
                    return res.status(404).send({ message: "User Not found." });
                }
                console.log(user.password);
                const passwordIsValid = bcrypt.compareSync(
                    req.body.password,
                    user.password
                );
                if (!passwordIsValid) {
                    return res.status(401).send({
                        accessToken: undefined,
                        message: "Invalid Password!"
                    });
                }
                console.log(user.repid);
                const token = jwt.sign({ username: req.body.username, repid: user.repid, scope: req.body.scope }, JWT_SECRET);
                res.status(200).send({ token: token });
            });
    }




}

