import { Request, Response } from "express";
import service from "../../services/getCustomers/service";

export class GetCustomersController {
    public async getCustomers(req: Request, res: Response): Promise<void> {
        const customers = await service.getCustomers(req);
        res.json({ customers });
    }
}