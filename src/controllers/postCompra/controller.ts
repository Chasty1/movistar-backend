import { Request, Response } from "express";
import service from "../../services/postCompra/service";

const postSOSRoute = async (req: Request, res: Response) => {
    await service.postCompra(req).then(response => {
        if (response) {
            res.status(200).send({ status: "Replace Offer ok. Pending activation" });
        } else {
            res.status(404).send({ "status": "Item Not Found" });
        }

    });
};

export default { postSOSRoute };
