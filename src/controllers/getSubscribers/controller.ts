import { Request, Response } from "express";
import service from "../../services/getSubscribers/service";

export class GetSubscribersController {
    public async getSubscribers(req: Request, res: Response): Promise<void> {
        const subscribers = await service.getSubscribers(req);
        res.json({ subscribers });
    }
}