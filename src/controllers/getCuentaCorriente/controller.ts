import { Request, Response } from "express";
import service from "../../services/getCuentaCorriente/service";


export class GetCuentaCorrienteController {
    public async getCuentaCorriente(req: Request, res: Response): Promise<void> {
        const cuentaCorriente = await service.getCuentaCorriente(req);
        res.json({ cuentaCorriente });
    }
}
