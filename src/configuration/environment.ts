export const environment = {
    port: process.env.PORT || 8080,
    TOKEN_URI: process.env.TOKEN_URI,
    IBM_CLIENT: process.env.IBM_CLIENT,
    API_URI: process.env.API_URI,
    CLIENT_ID: process.env.CLIENT_ID,
    CLIENT_SECRET: process.env.CLIENT_SECRET,
    CONTENT_TYPE: process.env.CONTENT_TYPE,
    APP: process.env.APP
};
